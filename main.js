#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
const readline = require('readline');

const notesFilePath = path.join(__dirname, 'notes.json');

// Load existing notes or initialize an empty array
let notes = [];
if (fs.existsSync(notesFilePath)) {
    notes = JSON.parse(fs.readFileSync(notesFilePath, 'utf8'));
}

// Command-line interface setup
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

function saveNotes() {
    fs.writeFileSync(notesFilePath, JSON.stringify(notes, null, 2), 'utf8');
}

function addNote() {
    // Get args if not present then throw error
    if (process.argv.length < 4) {
        console.log('Usage: node main.js add [note]')
        rl.close();
        return;
    }

    const note = process.argv.slice(3).join(' ');
    if (!note) {
        console.log('Please provide a note to add.');
        rl.close();
        return;
    }

    notes.push({ note });
    saveNotes();
    console.log('Note added.');
    rl.close();
}

function listNotes() {
    notes.forEach((note, index) => {
        console.log(`${index + 1}. ${note.note}`);
    });
    rl.close();
}

function main() {
    if (process.argv.length < 3) {
        console.log('Usage: <command> [arguments]');
        rl.close();
        return;
    }

    const command = process.argv[2];

    switch (command) {
        case 'add':
            addNote();
            break;
        case 'list':
            listNotes();
            break;
        default:
            console.log('Unknown command');
            rl.close();
            break;
    }
}

main();
